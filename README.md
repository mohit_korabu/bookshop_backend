# Bookshop JJ

## Pre Requisite

1. Java 11
2. Gradle

## Run without IDE
`cd bookshop-jj`

`./gradlew build`

`java -jar build/libs/bookshop-jj-0.0.1-SNAPSHOT.jar`

## Packaging
`docker build -t bookshop-jj .`

`docker run --rm -d --name bookshop -p 8080:8080 bookshop-jj:latest`

## Database
Access the H2 DB via - `http://localhost:8080/h2-console`
Or Postgresql

## Checkstyle

## Configure Intellij
1. Preferences -> Plugins -> checkstyle intellij idea plugin -> Update it to the latest version
2. Preferences -> Tools -> Checkstyle (search for it) -> Click + under Configuration File
3. Give any name. E.g.: WS-Checkstyle
4. Select "Use a local checkstyle file". Select the file - 'src/main/resources/checkstyle.xml'
5. Select this new checkstyle and save
6. Re-import gradle (right bar, select gradle, and hit refresh/reload icon)