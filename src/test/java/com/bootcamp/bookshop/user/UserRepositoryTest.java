package com.bootcamp.bookshop.user;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class UserRepositoryTest {

    private final UserRepository userRepository;

    @Autowired
    public UserRepositoryTest(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Test
    void shouldSaveAUser() {
        final User user = new User("a@b.com", "pwd", Role.USER);
        final User savedUser = userRepository.save(user);

        assertTrue(userRepository.findById(savedUser.getId()).isPresent());
    }
}
