package com.bootcamp.bookshop.user;

import static com.bootcamp.bookshop.user.UserTestBuilder.buildSignInUserCommand;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Optional;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private Validator validator;

    @InjectMocks
    private final UserService userService = new UserService();

    @Test
    void shouldCreateUserWithValidInputs() throws InvalidEmailException, InvalidPasswordFormatException {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().build();
        final User user = new UserTestBuilder().withEmail(userCredentials.getEmail()).build();
        when(userRepository.save(any(User.class))).thenReturn(user);

        final User createdUser = userService.create(userCredentials);

        final ArgumentCaptor<User> argCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository, times(1)).save(argCaptor.capture());
        assertEquals(userCredentials.getEmail(), argCaptor.getValue().getEmail());
        assertEquals(user.getId(), createdUser.getId());
        assertEquals(user.getEmail(), createdUser.getEmail());
    }

    @Test
    void shouldNotCreateUserWhenUserWithSameEmailAlreadyExists() throws InvalidPasswordFormatException {
        final CreateUserCommand userCommand = new CreateUserCommandTestBuilder().build();
        when(userRepository.findByEmail(userCommand.getEmail())).thenReturn(Optional.of(new User()));
        userRepository.save(User.create(userCommand));

        final InvalidEmailException createUserException = assertThrows(InvalidEmailException.class,
                () -> userService.create(userCommand));
        assertEquals("User with same email already created", createUserException.getMessage());
    }

    @Test
    void shouldNotCreateUserWhenInputIsInvalid() {
        final CreateUserCommand invalidCommand = new CreateUserCommandTestBuilder().withEmptyEmail().build();
        when(validator.validate(any(User.class))).thenThrow(ConstraintViolationException.class);

        assertThrows(ConstraintViolationException.class, () -> userService.create(invalidCommand));
    }

    @Test
    void shouldSignInUserWhenEmailAndPasswordAreValid() throws IncorrectPasswordException {
        final SignInUserCommand signInUserCommand = buildSignInUserCommand();
        final User expectedUser = new UserTestBuilder()
                .withEmail("testemail@test.com")
                .withPassword("$2a$10$9mBb9M9rxuGUzAzIKB7XiuQaGBRSBbcb6F/aqQeW.cmGSqHkI2qgi").build();
        when(userRepository.findByEmail(signInUserCommand.getEmail())).thenReturn(Optional.of(expectedUser));

        final User user = userService.signIn(signInUserCommand);

        verify(userRepository).findByEmail(signInUserCommand.getEmail());
        assertEquals(signInUserCommand.getEmail(), user.getEmail());
    }

    @Test
    void shouldThrowIncorrectPasswordExceptionWhenPasswordIsInvalid() {
        final SignInUserCommand signInUserCommand = new SignInUserCommand("testemail@test.com",
                "invalid-password", Role.USER);

        final User expectedUser = new UserTestBuilder()
                .withEmail("testemail@test.com")
                .withPassword("$2a$10$9mBb9M9rxuGUzAzIKB7XiuQaGBRSBbcb6F/aqQeW.cmGSqHkI2qgi").build();
        when(userRepository.findByEmail(signInUserCommand.getEmail())).thenReturn(Optional.of(expectedUser));

        assertThrows(IncorrectPasswordException.class, () -> userService.signIn(signInUserCommand));
    }

    @Test
    void shouldThrowUsernameNotFoundExceptionWhenEmailIsInvalid() {
        final SignInUserCommand signInUserCommand = new SignInUserCommand("testemail@test.com",
                "invalid-password", Role.USER);

        when(userRepository.findByEmail(signInUserCommand.getEmail())).thenReturn(Optional.empty());

        final UsernameNotFoundException exception = assertThrows(UsernameNotFoundException.class,
                () -> userService.signIn(signInUserCommand));
        assertEquals("User not found", exception.getMessage());
    }

    @Test
    void shouldBeUserWithAdminRoleWhenEmailBelongsToAdminUser() {
        final User user = new UserTestBuilder()
                .withEmail("admin@tw.com")
                .withPassword("foobar").withRole(Role.ADMIN).build();
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));

        final UserDetails userDetails = userService.loadUserByUsername("admin@tw.com");

        assertIterableEquals(AuthorityUtils.createAuthorityList("ROLE_ADMIN"), userDetails.getAuthorities());
    }

    @Test
    void shouldBeUserWithUserRoleWhenEmailBelongsToNormalUser() {
        final User user = new UserTestBuilder()
                .withEmail("user@tw.com")
                .withPassword("foobar").withRole(Role.USER).build();
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));

        final UserDetails userDetails = userService.loadUserByUsername("user@tw.com");

        assertIterableEquals(AuthorityUtils.createAuthorityList("ROLE_USER"), userDetails.getAuthorities());
    }
}
