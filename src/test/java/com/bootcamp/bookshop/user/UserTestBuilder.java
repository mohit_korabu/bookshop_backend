package com.bootcamp.bookshop.user;

import com.bootcamp.bookshop.user.User.UserBuilder;

public class UserTestBuilder {
    private final UserBuilder userBuilder;

    public UserTestBuilder() {
        userBuilder = User.builder()
                .id(1L)
                .email("testemail@test.com")
                .password("foobar")
                .role(Role.USER);
    }

    public static CreateUserCommand buildCreateUserCommand() {
        return new CreateUserCommandTestBuilder().build();
    }

    public static SignInUserCommand buildSignInUserCommand() {
        return new SignInUserCommand("testemail@test.com", "foobar", Role.USER);
    }

    public User build() {
        return userBuilder.build();
    }

    public UserTestBuilder withEmail(final String email) {
        userBuilder.email(email);
        return this;
    }

    public UserTestBuilder withPassword(final String password) {
        userBuilder.password(password);
        return this;
    }

    public UserTestBuilder withRole(final Role role) {
        userBuilder.role(role);
        return this;
    }
}
