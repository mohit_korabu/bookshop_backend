package com.bootcamp.bookshop.user;

public class CreateSignInCommandTestBuilder {
    private final SignInUserCommand.SignInUserCommandBuilder commandBuilder;

    public CreateSignInCommandTestBuilder() {
        commandBuilder = SignInUserCommand.builder()
                .email("testemail@test.com")
                .password("Foobar@123")
                .role(Role.USER);
    }

    SignInUserCommand build() {
        return commandBuilder.build();
    }

    public CreateSignInCommandTestBuilder withAdminEmail() {
        commandBuilder.role(Role.ADMIN);
        return this;
    }
}
