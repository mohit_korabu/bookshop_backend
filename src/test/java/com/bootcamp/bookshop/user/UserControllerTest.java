package com.bootcamp.bookshop.user;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Set;
import javax.validation.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


@WebMvcTest(value = UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Validator validator;

    @Test
    void shouldCreateUserWhenCredentialsAreValid() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().build();
        final User user = new UserTestBuilder().withEmail(userCredentials.getEmail()).build();
        when(userService.create(userCredentials)).thenReturn(user);
        final UserView userView = UserView.builder()
                .id(user.getId().toString())
                .email(userCredentials.getEmail())
                .build();

        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(objectMapper.writeValueAsString(userView)));

        verify(userService, times(1)).create(userCredentials);
    }

    @Test
    void shouldRespondWithErrorMessageWhenCreateUserFails() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().build();
        when(userService.create(userCredentials)).thenThrow(new InvalidEmailException());

        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void shouldRespondWithErrorMessageWhenCreateUserValidationFails() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().withEmptyEmail().build();
        final Set<ConstraintViolation<User>> violations = validator.validate(User.create(userCredentials));
        when(userService.create(userCredentials)).thenThrow(new ConstraintViolationException(violations));

        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.errors.email").value("Email is mandatory"));
    }

    @Test
    void shouldRespondWithErrorMessageWhenInvalidEmailFormat() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().withInvalidEmail().build();
        final Set<ConstraintViolation<User>> violations = validator.validate(User.create(userCredentials));
        when(userService.create(userCredentials)).thenThrow(new ConstraintViolationException(violations));

        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.errors.email").value("must be a well-formed email address"));
    }

    @Test
    void shouldRespondWithErrorMessageWhenPasswordEmpty() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().withEmptyPassword().build();
        when(userService.create(userCredentials)).thenThrow(new InvalidPasswordFormatException());

        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void shouldRespondWithErrorMessageWhenPasswordInvalid() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().withInvalidPassword().build();
        when(userService.create(userCredentials)).thenThrow(new InvalidPasswordFormatException());
        mockMvc.perform(post("/users")
                        .content(objectMapper.writeValueAsString(userCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void shouldLoginUserAndRedirectToBookList() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().build();
        final SignInUserCommand signInCredentials = new CreateSignInCommandTestBuilder().build();
        final User user = new UserTestBuilder().withEmail(userCredentials.getEmail()).build();
        when(userService.create(userCredentials)).thenReturn(user);
        when(userService.signIn(signInCredentials)).thenReturn(user);

        mockMvc.perform(post("/users/login")
                        .content(objectMapper.writeValueAsString(signInCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(redirectedUrl("/books"));

    }

    @Test
    void shouldLoginAdminAndRedirectToAdminDashboard() throws Exception {
        final CreateUserCommand userCredentials = new CreateUserCommandTestBuilder().build();
        final SignInUserCommand signInCredentials = new CreateSignInCommandTestBuilder().withAdminEmail().build();
        final User user = new UserTestBuilder().withEmail(userCredentials.getEmail()).withRole(Role.ADMIN).build();
        when(userService.create(userCredentials)).thenReturn(user);
        when(userService.signIn(signInCredentials)).thenReturn(user);

        mockMvc.perform(post("/users/login")
                        .content(objectMapper.writeValueAsString(signInCredentials))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(redirectedUrl("/admin"));
    }

    private Validator constraintsValidator() {
        final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }

}
