package com.bootcamp.bookshop.user;

import com.bootcamp.bookshop.user.CreateUserCommand.CreateUserCommandBuilder;

public class CreateUserCommandTestBuilder {
    private final CreateUserCommandBuilder commandBuilder;

    public CreateUserCommandTestBuilder() {
        commandBuilder = CreateUserCommand.builder()
                .email("testemail@test.com")
                .password("Foobar@123");
    }

    CreateUserCommand build() {
        return commandBuilder.build();
    }

    public CreateUserCommandTestBuilder withEmptyEmail() {
        commandBuilder.email("");
        return this;
    }

    public CreateUserCommandTestBuilder withEmptyPassword() {
        commandBuilder.password("");
        return this;
    }

    public CreateUserCommandTestBuilder withInvalidEmail() {
        commandBuilder.email("aaaaa.com");
        return this;
    }

    public CreateUserCommandTestBuilder withInvalidPassword() {
        commandBuilder.email("aswdaaa@b.com");
        commandBuilder.password("a1s@1sdasds");
        return this;
    }
}
