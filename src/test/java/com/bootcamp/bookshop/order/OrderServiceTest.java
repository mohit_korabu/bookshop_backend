package com.bootcamp.bookshop.order;

import static org.junit.jupiter.api.Assertions.*;

import com.bootcamp.bookshop.book.Book;
import com.bootcamp.bookshop.book.BookRepository;
import com.bootcamp.bookshop.book.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderServiceTest {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private OrderService orderService;
    @Autowired
    private BookService bookService;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    void tearDownOrder() {
        orderRepository.deleteAll();
    }

    @AfterEach
    void tearDownBook() {
        bookRepository.deleteAll();
    }

    @Test
    void shouldCreateOrder() throws Exception {
        final Book book = new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha");
        bookRepository.save(book);
        final Long id = bookRepository.findByIsbn("3456").get().getId();
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", id, 5);
        orderService.create(order);

        final List<Order> orderList = orderService.fetchAll();
        assertEquals(1, orderList.size());
        assertEquals("Prayansh", orderList.get(0).getName());
    }

    @Test
    void shouldThrowExceptionWhenBookQuantityIsNotAvailable() throws Exception {
        final Book book = new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha");
        final Long id = bookRepository.save(book).getId();
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", id, 300);

        try {
            orderService.create(order);
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    void shouldThrowExceptionWhenBookIsInvalid() throws Exception {
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 3L, 300);
        try {
            orderService.create(order);
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    void shouldConfirmOrder() throws Exception {
        final Book book = new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha");
        final Long id = bookRepository.save(book).getId();
        final Order order = new Order("Prayansh", "xyz@q.com", "989682218", "abc", "xyz", id, 100, false);
        final Long orderId = orderRepository.save(order).getId();
        orderService.confirmOrder(orderId);
        Assertions.assertEquals(true, orderRepository.findById(orderId).get().isConfirmation());
    }

    @Test
    void shouldThrowExceptionIfOrderAlreadyConfirmed() throws Exception {
        final Book book = new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha");
        final Long id = bookRepository.save(book).getId();
        final Order order = new Order("Prayansh", "xyz@q.com", "989682218", "abc", "xyz", id, 100, false);
        final Long orderId = orderRepository.save(order).getId();
        orderService.confirmOrder(orderId);
        try {
            orderService.confirmOrder(orderId);
            fail();
        } catch (Exception e) {
            Assertions.assertNotNull(e);
        }
    }

    @Test
    void shouldThrowExceptionIfBookOutOfStock() throws Exception {

        final Book book = new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha");
        final Long id = bookRepository.save(book).getId();
        final CreateOrderCommand simultaneousOrder1 = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", id, 100);
        final CreateOrderCommand simultaneousOrder2 = new CreateOrderCommand("Venkatesh", "xyz@q.com",
                "989682218", "abc", "xyz", id, 100);

        orderService.create(simultaneousOrder1);
        orderService.create(simultaneousOrder2);
        final Long orderId1 = orderRepository.findByName(simultaneousOrder1.getName()).get().getId();
        final Long orderId2 = orderRepository.findByName(simultaneousOrder2.getName()).get().getId();
        orderService.confirmOrder(orderId1);
        try {
            orderService.confirmOrder(orderId2);
            fail();
        } catch (Exception e) {
            System.out.println(e);
            Assertions.assertNotNull(e);
        }
    }

    @Test
    void shouldFetchOrderByID() throws Exception {
        final Order order = new Order("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 1L,
                5,false);
        final Book book = new Book("title", "author name", 300);

        final Order savedOrder = orderRepository.save(order);
        final Book savedBook = bookRepository.save(book);
        final OrderSummaryView orderActual = orderService.findOrderById(savedOrder.getId());

        assertEquals(order.getName(), orderActual.getName());
    }
}


