package com.bootcamp.bookshop.order;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bootcamp.bookshop.book.Book;
import com.bootcamp.bookshop.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@WithMockUser
@WebMvcTest(OrderController.class)
public class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @MockBean
    private UserService userService;

    @Test
    void shouldCreateOrder() throws Exception {
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 3L, 5);

        when(orderService.create(any())).thenReturn(any());

        mockMvc.perform(post("/books/order")
                .content(objectMapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        verify(orderService, times(1)).create(any());
    }

    @Test
    void shouldNotCreateOrderWhenBookNotAvailable() throws Exception {
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 3L, 100);

        doThrow(new Exception("Exception")).when(orderService).create(any());


        mockMvc.perform(post("/books/order")
                .content(objectMapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();

        verify(orderService, times(1)).create(any());

    }

    @Test
    void shouldThrowExceptionWhenBookIsInvalid() throws Exception {
        final CreateOrderCommand order = new CreateOrderCommand("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 3L, 100);

        doThrow(new Exception("Exception")).when(orderService).create(any());


        mockMvc.perform(post("/books/order")
                .content(objectMapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();

        verify(orderService, times(1)).create(any());

    }

    @Test
    void shouldConfirmOrder() throws Exception {
        doNothing().when(orderService).confirmOrder(5L);

        mockMvc.perform(put("/books/order/5/confirm")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        verify(orderService, times(1)).confirmOrder(5L);
    }

    @Test
    void shouldThrowExceptionWhenOrderAlreadyConfirmed() throws Exception {
        doThrow(new Exception("Exception")).when(orderService).confirmOrder(any());

        mockMvc.perform(put("/books/order/5/confirm")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();

        verify(orderService, times(1)).confirmOrder(5L);
    }

    @Test
    void shouldThrowExceptionWhenBookQualityNotAvailable() throws Exception {
        doThrow(new Exception("Exception")).when(orderService).confirmOrder(any());

        mockMvc.perform(put("/books/order/5/confirm")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();

        verify(orderService, times(1)).confirmOrder(5L);
    }

    @Test
    void shouldGetOrderById() throws Exception {

        final Order order = new Order("Prayansh", "xyz@q.com",
                "989682218", "abc", "xyz", 3L, 5);
        final Book book = new Book("title", "author name", 300);

        when(orderService.findOrderById(1)).thenReturn(new OrderSummaryView(order,book));

        final MvcResult mvcResult = mockMvc.perform(get("/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        verify(orderService, times(1)).findOrderById(1);
        Assertions.assertEquals(objectMapper.writeValueAsString(new OrderSummaryView(order,book)),
                mvcResult.getResponse().getContentAsString());

    }


}
