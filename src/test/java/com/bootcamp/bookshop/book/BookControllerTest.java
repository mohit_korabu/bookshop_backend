package com.bootcamp.bookshop.book;

import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bootcamp.bookshop.exception.BookException;
import com.bootcamp.bookshop.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@WithMockUser
@WebMvcTest(BookController.class)
class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BookService bookService;

    @MockBean
    UserService userService;

    @Test
    void shouldListAllBooksWhenPresent() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book = new Book("title", "author name", 300);
        books.add(book.toView());
        when(bookService.fetchAllBookView("price","desc",1,10)).thenReturn(books);

        mockMvc.perform(get("/books")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1));
        verify(bookService, times(1)).fetchAllBookView("price","desc",1,10);
    }

    @Test
    void shouldBeEmptyListWhenNoBooksPresent() throws Exception {
        when(bookService.fetchAllBookView("price","desc",1,10)).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/books")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(0));
        verify(bookService, times(1)).fetchAllBookView("price","desc",1,10);
    }

    @Test
    void shouldListAllBooksWhenPresentByPriceDescending() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("title1", "author name2", 400);
        books.add(book2.toView());
        books.add(book1.toView());
        when(bookService.fetchAllBookView("price","desc",1,10)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=desc")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
        verify(bookService, times(1)).fetchAllBookView("price","desc",1,10);

    }

    @Test
    void shouldListAllBooksWhenPresentByPriceAscending() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("title1", "author name2", 400);
        books.add(book1.toView());
        books.add(book2.toView());
        when(bookService.fetchAllBookView("price","asc",1,10)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=asc")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
        verify(bookService, times(1)).fetchAllBookView("price","asc",1,10);

    }

    @Test
    void shouldListAllBooksWhenPresentByPriceAscending2() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("java", "pawan", 400);
        final Book book3 = new Book("spring", "abhishek", 350);
        books.add(book1.toView());
        books.add(book3.toView());
        books.add(book2.toView());
        when(bookService.fetchAllBookView("price","asc",1,10)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=asc")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
        verify(bookService, times(1)).fetchAllBookView("price","asc",1,10);

    }

    @Test
    void shouldListAllBooksWhenPresentByPriceAscending3() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("java", "pawan", 400);
        final Book book3 = new Book("spring", "abhishek", 350);
        books.add(book1.toView());
        books.add(book3.toView());
        books.add(book2.toView());
        when(bookService.fetchAllBookView("price","asc",1,10)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=asc&pageNumber=1&pageSize=10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
        verify(bookService, times(1)).fetchAllBookView("price","asc",1,10);
    }

    @Test
    void shouldList2BooksInPage1InAscendingOrderOfPrice() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("java", "pawan", 400);
        final Book book3 = new Book("spring", "abhishek", 350);
        books.add(book1.toView());
        books.add(book3.toView());
        books.add(book2.toView());
        when(bookService.fetchAllBookView("price","asc",1,2)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=asc&pageNumber=1&pageSize=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
        verify(bookService, times(1)).fetchAllBookView("price","asc",1,2);
    }

    @Test
    void shouldList2BooksInPage1InDescendingOrderOfPrice() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book book1 = new Book("title", "author name", 300);
        final Book book2 = new Book("java", "pawan", 400);
        final Book book3 = new Book("spring", "abhishek", 350);
        books.add(book2.toView());
        books.add(book3.toView());
        books.add(book1.toView());
        when(bookService.fetchAllBookView("price","desc",1,2)).thenReturn(books);

        mockMvc.perform(get("/books?sortBy=price&order=desc&pageNumber=1&pageSize=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
        verify(bookService, times(1)).fetchAllBookView("price","desc",1,2);
    }

    @Test
    void shouldFindByBookNameKeyWord() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book javaBook = new Book("java", "sam", 300);
        books.add(javaBook.toView());
        when(bookService.findAllByAttributes("java","price","desc",1,10)).thenReturn(books);

        final String responseString = mockMvc.perform(get("/books?search=java")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertEquals(objectMapper.writeValueAsString(books),responseString);
    }

    @Test
    void shouldGetAllBooksIfKeywordNotFound() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book javaBook = new Book("java", "sam", 300);
        final Book cppBook = new Book("cpp", "sim", 400);
        books.add(javaBook.toView());
        books.add(cppBook.toView());
        when(bookService.findAllByAttributes("python","price","desc",1,10)).thenReturn(books);

        final String responseString = mockMvc.perform(get("/books?search=python")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertEquals(objectMapper.writeValueAsString(books), responseString);
    }

    @Test
    void shouldFindByBookNameKeyWordAndListOnly2BooksInAPage() throws Exception {
        final List<BookView> books = new ArrayList<>();
        final Book javaBook1 = new Book("java1", "sam", 300);
        final Book javaBook2 = new Book("java2", "tom", 400);
        final Book javaBook3 = new Book("java3", "jam", 200);
        books.add(javaBook2.toView());
        books.add(javaBook1.toView());
        books.add(javaBook3.toView());
        when(bookService.findAllByAttributes("java","price","desc",1,2)).thenReturn(books);

        final String responseString = mockMvc.perform(get("/books?search=java&pageSize=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertEquals(objectMapper.writeValueAsString(books),responseString);
    }


    @Test
    void shouldCreateBooks() throws Exception {
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 200,
                " ", " ", 123, "3456", "3457", "2019",
                " ", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                " ", " ", 123, "34556", "34537", "2019",
                " ", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        doNothing().when(bookService).create(any());

        mockMvc.perform(post("/admin/books")
                .content(objectMapper.writeValueAsString(books))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(bookService, times(1)).create(any());
    }

    @Test
    void shouldReturnExceptionIfBookFailsToCreate() throws Exception {
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 200,
                " ", " ", 123, "3456", "3457", "2019",
                " ", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                " ", " ", 12, "38756", "3987", "2019",
                " ", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        doThrow(new BookException("Exception For book : "
                + objectMapper.writeValueAsString(book1))).when(bookService).create(any());
        System.out.println(objectMapper.writeValueAsString(books));
        mockMvc.perform(post("/admin/books")
                .content(objectMapper.writeValueAsString(books))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());

        verify(bookService, times(1)).create(any());
    }

    @Test
    void shouldGetBookById() throws Exception {

        final Book book = new Book("title", "author name", 300);
        when(bookService.findBookById(1)).thenReturn(Optional.of(book));

        final MvcResult mvcResult = mockMvc.perform(get("/books/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        verify(bookService, times(1)).findBookById(1);
        Assertions.assertEquals(objectMapper.writeValueAsString(book),mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void shouldReturnNullWhenBookNotPresent() throws Exception {
        final Book book = new Book("title", "author name", 300);
        when(bookService.findBookById(1)).thenReturn(Optional.of(book));

        final MvcResult mvcResult = mockMvc.perform(get("/books/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        verify(bookService, times(1)).findBookById(10);
        Assertions.assertEquals(objectMapper.writeValueAsString(null),mvcResult.getResponse().getContentAsString());
    }
}
