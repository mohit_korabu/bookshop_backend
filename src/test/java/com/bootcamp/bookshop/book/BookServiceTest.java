package com.bootcamp.bookshop.book;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

import com.bootcamp.bookshop.exception.BookException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BookServiceTest {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookService bookService;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    void tearDown() {
        bookRepository.deleteAll();
    }

    @Test
    void shouldFetchAllBooks() {
        final Book book = new Book("title", "author name", 300);
        bookRepository.save(book);

        final List<BookView> books = bookService.fetchAllBookView("price","desc",1,10);

        assertEquals(1, books.size());
        assertEquals("title", books.get(0).getName());
    }

    @Test
    void shouldFetchAllBooksBeSortedByPriceDesc() {
        final Book lowPrice = new Book("title", "author name", 300);
        final Book mediumPrice = new Book("medium", "author2", 350);
        final Book highPrice = new Book("costlier", "author name", 400);
        bookRepository.save(lowPrice);
        bookRepository.save(mediumPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","desc",1,10);

        assertEquals(3, books.size());
        assertEquals("costlier", books.get(0).getName());
        assertEquals("title", books.get(2).getName());
    }

    @Test
    void shouldFetchAllBooksBeSortedByPriceDesc2Books() {
        final Book lowPrice = new Book("physics", "abhishek", 1000);
        final Book highPrice = new Book("chemistry", "mohit", 5000);
        bookRepository.save(lowPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","desc",1,10);

        assertEquals(2, books.size());
        assertEquals("mohit", books.get(0).getAuthorName());
        assertEquals("abhishek", books.get(1).getAuthorName());
    }

    @Test
    void shouldFetchAllBooksBeSortedByPriceAsc() {
        final Book lowPrice = new Book("title", "author name", 300);
        final Book mediumPrice = new Book("medium", "author2", 350);
        final Book highPrice = new Book("costlier", "author name", 400);
        bookRepository.save(lowPrice);
        bookRepository.save(mediumPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","asc",1,10);

        assertEquals(3, books.size());
        assertEquals("title", books.get(0).getName());
        assertEquals("costlier", books.get(2).getName());
    }

    @Test
    void shouldFetchAllBooksBeSortedByPriceAsc2Books() {
        final Book lowPrice = new Book("physics", "abhishek", 1000);
        final Book highPrice = new Book("chemistry", "mohit", 5000);
        bookRepository.save(lowPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","asc",1,10);

        assertEquals(2, books.size());
        assertEquals("mohit", books.get(1).getAuthorName());
        assertEquals("abhishek", books.get(0).getAuthorName());
    }

    @Test
    void shouldGetListOfBooksByName() {
        final Book lowPrice = new Book("title", "author1", 300);
        final Book highPrice = new Book("costlier", "author name", 400);
        bookRepository.save(lowPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.findAllByAttributes("title","price","desc",1,10);
        assertEquals("title", books.get(0).getName());
    }

    @Test
    void shouldGetListOfBooksByAuthorName() {
        final Book lowPrice = new Book("title", "author1", 300);
        final Book highPrice = new Book("costlier", "author name", 400);
        bookRepository.save(lowPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.findAllByAttributes("title","price","desc",1,10);
        assertEquals("title", books.get(0).getName());
    }

    @Test
    void shouldGetListOfBooksByPrice() {
        final Book lowPrice = new Book("title", "author1", 300);
        final Book highPrice = new Book("costlier", "author name", 400);
        bookRepository.save(lowPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.findAllByAttributes("400","price","desc",1,10);
        assertEquals("costlier", books.get(0).getName());
    }

    @Test
    void shouldGetListOfBooksByPriceKeyword() {
        final Book javaBook = new Book("java", "arthur", 300);
        final Book cppBook = new Book("cpp", "bjorne", 400);
        bookRepository.save(javaBook);
        bookRepository.save(cppBook);

        final List<BookView> books = bookService.findAllByAttributes("400","price","desc",1,10);
        assertTrue(cppBook.toView(cppBook.getId()).equals(books.get(0)));
    }

    @Test
    void shouldGetListOfBooksByRandomKeyword() {
        final Book javaBook = new Book("java", "arthur", 300);
        final Book cppBook = new Book("cpp", "bjorne", 400);
        bookRepository.save(javaBook);
        bookRepository.save(cppBook);

        final List<BookView> books = bookService.findAllByAttributes("jo","price","desc",1,10);
        assertEquals("cpp", books.get(0).getName());
    }

    @Test
    void shouldGetListOfBooksByRandomCapitalKeyword() {
        final Book javaBook = new Book("java", "arthur", 300);
        final Book cppBook = new Book("cpp", "bjorne", 400);
        bookRepository.save(javaBook);
        bookRepository.save(cppBook);

        final List<BookView> books = bookService.findAllByAttributes("JO","price","desc",1,10);
        assertEquals("cpp", books.get(0).getName());
    }

    @Test
    void shouldGetListOfBooksByAuthorKeyword() {
        final Book javaBook = new Book("java", "arthur", 300);
        final Book cppBook = new Book("cpp", "bjourne", 400);
        final Book pythonBook = new Book("python", "thomson", 500);
        bookRepository.save(javaBook);
        bookRepository.save(cppBook);
        bookRepository.save(pythonBook);
        final List<BookView> expectedList = new ArrayList<>(asList(javaBook.toView(javaBook.getId()),
                cppBook.toView(cppBook.getId())));
        final List<BookView> actualList = bookService.findAllByAttributes("ur","price","asc",1,10);

        assertTrue(expectedList.equals(actualList));
    }

    @Test
    void shouldNotGetAnyBookIfKeywordNotFound() {
        final Book javaBook = new Book("java", "arthur", 300);
        final Book cppBook = new Book("cpp", "bjourne", 400);
        final Book pythonBook = new Book("python", "thomson", 500);
        bookRepository.save(javaBook);
        bookRepository.save(cppBook);
        bookRepository.save(pythonBook);
        final List<BookView> expectedList = new ArrayList<>();
        final List<BookView> actualList = bookService.findAllByAttributes("javascript","price","asc",1,10);

        assertTrue(expectedList.equals(actualList));
    }

    @Test
    void shouldGetListOfBooksByAuthorKeywordAndOnly2InAPage() {
        final Book javaBook1 = new Book("Java1", "arthur", 300);
        final Book javaBook2 = new Book("java2", "bjourne", 400);
        final Book javaBook3 = new Book("java3", "thomson", 500);
        bookRepository.save(javaBook1);
        bookRepository.save(javaBook2);
        bookRepository.save(javaBook3);
        final List<BookView> expectedList = new ArrayList<>(asList(javaBook3.toView(javaBook3.getId())));
        final List<BookView> actualList = bookService.findAllByAttributes("java","price","asc",2,2);

        assertTrue(expectedList.equals(actualList));
    }


    @Test
    void shouldCreateBooks() throws Exception {
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34676", "32457",
                "2019", "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        bookService.create(books);

        final List<Book> bookList = bookService.fetchAll();
        assertEquals(2, bookList.size());
        assertEquals("name1", bookList.get(1).getName());
    }

    @Test
    void shouldUpdatePriceIfExists() throws Exception {
        bookRepository.save(new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha"));
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 500,
                "Null", "Null", 123, "3456", "3457", "2019",
                "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34556", "34537", "2019",
                "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        bookService.create(books);

        final List<Book> bookList = bookService.fetchAll();
        assertEquals(2, bookList.size());
        System.out.println(bookList);
        assertEquals(500, bookList.get(0).getPrice());
    }

    @Test
    void shouldFetchBooksPriceDesc() throws Exception {
        bookRepository.save(new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha"));
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 500,
                "Null", "Null", 123, "3456", "3457", "2019",
                "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34556", "34537", "2019",
                "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        bookService.create(books);

        final List<Book> bookList = bookService.fetchAll("price", "desc");
        assertEquals(2, bookList.size());
        assertEquals(500, bookList.get(0).getPrice());
    }

    @Test
    void shouldFetchBooksPriceAsc() throws Exception {
        bookRepository.save(new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha"));
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 500,
                "Null", "Null", 123, "3456", "3457", "2019",
                "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34556", "34537", "2019",
                "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        bookService.create(books);

        final List<Book> bookList = bookService.fetchAll("price", "asc");
        assertEquals(2, bookList.size());
        assertEquals(300, bookList.get(0).getPrice());
    }

    @Test
    void shouldThrowExceptionWithBooksNameIfCreateBooksFails() throws Exception {
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 0,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34556", "34537",
                "2019", "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);

        try {
            bookService.create(books);
            fail();
        } catch (BookException e) {
            assertEquals(e, new BookException("Exception For book : " + objectMapper.writeValueAsString(book1)));
        }
    }

    @Test
    void shouldThrowExceptionWithBooksNameIfUpdateBooksFails() throws Exception {
        bookRepository.save(new Book("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F, "gaha"));
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 0,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 123, "34556", "34537",
                "2019", "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);

        try {
            bookService.create(books);
            fail();
        } catch (Exception e) {
            assertEquals(e, new BookException("Exception For book : " + objectMapper.writeValueAsString(book1)));
        }
    }

    @Test
    void shouldRollbackAllChangesIfThrowException() throws Exception {
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 200,
                "Null", "Null", 123, "3456", "3457",
                "2019", "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 0,
                "Null", "Null", 123, "34556", "34537",
                "2019", "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);

        try {
            bookService.create(books);
            fail();
        } catch (BookException e) {
            final List<Book> bookList = bookRepository.findAll();
            assertEquals(0, bookList.size());
            assertEquals(e, new BookException("Exception For book : " + objectMapper.writeValueAsString(book1)));
        }
    }

    @Test
    void shouldAddBookIfExists() throws Exception {
        bookRepository.save(new Book("name1", "author1", 200, "Null", "Null",
                100, "3456", "3457", "2019", "Null",
                "EN", 3.60F, ""));
        final CreateBookCommand book1 = new CreateBookCommand("name1", "author1", 500,
                "Null", "Null", 100, "3456", "3457", "2019",
                "Null", "EN", 3.60F);
        final CreateBookCommand book2 = new CreateBookCommand("name2", "author2", 300,
                "Null", "Null", 100, "34556", "34537", "2019",
                "Null", "EN", 3.60F);
        final List<CreateBookCommand> books = asList(book1, book2);
        bookService.create(books);

        final List<Book> bookList = bookService.fetchAll();
        assertEquals(2, bookList.size());
        System.out.println(bookList);
        assertEquals(200, bookList.get(0).getBooksCount());
    }

    @Test
    void shouldFetchBookByID() {

        final Book book = new Book("title", "author name", 300);
        final Book savedBook = bookRepository.save(book);
        final Optional<Book> bookActual = bookService.findBookById(savedBook.getId());

        assertTrue(bookActual.isPresent());
        assertEquals("title", bookActual.get().getName());
    }


    @Test
    void should2BooksInAPageInAscendingOrderOfPrice() {
        final Book lowPrice = new Book("physics", "abhishek", 1000);
        final Book mediumPrice = new Book("medium", "author2", 3500);
        final Book highPrice = new Book("chemistry", "mohit", 5000);
        bookRepository.save(lowPrice);
        bookRepository.save(mediumPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","asc",1,2);

        assertEquals(2, books.size());
        assertEquals("author2", books.get(1).getAuthorName());
        assertEquals("abhishek", books.get(0).getAuthorName());
    }

    @Test
    void should2BooksInAPageInDescendingOrderOfPrice() {
        final Book lowPrice = new Book("physics", "abhishek", 1000);
        final Book mediumPrice = new Book("medium", "author2", 3500);
        final Book highPrice = new Book("chemistry", "pawan", 5000);
        bookRepository.save(lowPrice);
        bookRepository.save(mediumPrice);
        bookRepository.save(highPrice);

        final List<BookView> books = bookService.fetchAllBookView("price","desc",1,3);

        assertEquals(3, books.size());
        assertEquals("author2", books.get(1).getAuthorName());
        assertEquals("pawan", books.get(0).getAuthorName());
        assertEquals(1000, books.get(2).getPrice());
    }
}
