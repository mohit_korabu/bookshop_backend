alter table books add column image_url VARCHAR(255);
alter table books add column small_image_url VARCHAR(255);
alter table books add column books_count integer;
alter table books add column isbn VARCHAR(255);
alter table books add column isbn_a VARCHAR(255);
alter table books add column original_publication_year VARCHAR(255);
alter table books add column original_title VARCHAR(255);
alter table books add column language_code VARCHAR(255);

