DELETE FROM books WHERE name='Harper Lee' OR name='F. Scott Fitzgerald';
INSERT INTO books (name, author_name, price, image_url ,small_image_url ,books_count , isbn ,isbn_a ,original_publication_year,original_title ,language_code,ratings,reviews)
VALUES ('City of Bones (The Mortal Instruments, #1)', 'Cassandra Clare', 400,'https://images.gr-assets.com/books/1432730315m/256683.jpg','https://images.gr-assets.com/books/1432730315m/256683.jpg',178,'1416914285','9781416914280','2007','City of Bones','eng',4.12,'review');

INSERT Into books (name, author_name, price, image_url ,small_image_url ,books_count , isbn ,isbn_a ,original_publication_year,original_title ,language_code,ratings,reviews)
VALUES ('Stephenie Meyer','Eclipse (Twilight, #3)', 2355,'https://images.gr-assets.com/books/1361038355m/428263.jpg','https://images.gr-assets.com/books/1361038355s/428263.jpg',185,'316160202','9780316160210','2007','Eclipse','en-US',3.69,'review');