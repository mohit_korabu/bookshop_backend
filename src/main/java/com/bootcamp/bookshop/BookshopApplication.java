package com.bootcamp.bookshop;

import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookshopApplication {

    @Generated
    public static void main(final String[] args) {
        SpringApplication.run(BookshopApplication.class, args);
    }

}
