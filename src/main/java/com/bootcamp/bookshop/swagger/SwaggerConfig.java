package com.bootcamp.bookshop.swagger;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;

@Configuration
public class SwaggerConfig implements WebMvcConfigurer {
    private static final String PATH = "/api-docs/swagger-ui/";

    @Bean
    public SecurityConfiguration securityConfiguration() {
        return SecurityConfigurationBuilder.builder()
                .enableCsrfSupport(true)
                .build();
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("api-v1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bootcamp.bookshop"))
                .paths(PathSelectors.any())
                .build();
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", PATH);
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfo(
                "Book Shop Service",
                "API for Book Shop Service",
                "v1",
                "Terms of service",
                apiContact(),
                "License of API",
                "API license URL", Collections.emptyList());
    }

    @Bean
    public Contact apiContact() {
        return new Contact(
                "Contact Name",
                "Contact Url",
                ""
        );
    }
}







