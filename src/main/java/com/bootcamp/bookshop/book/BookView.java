package com.bootcamp.bookshop.book;

import lombok.Builder;
import lombok.Generated;
import lombok.Getter;

@Builder
@Getter

public class BookView {
    private final long id;
    private final String name;
    private final int price;
    private final String authorName;
    private final String imageUrl;


    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public int getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Generated
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BookView bookView = (BookView) o;
        return this.name == bookView.name && this.authorName == bookView.authorName
                && this.price == bookView.price && this.id == bookView.id;
    }
}
