package com.bootcamp.bookshop.book;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByOrderByPriceDesc();

    Optional<Book> findByName(String keyword);

    @Query(value = "select * from books b where lower(b.name) like %?1% or lower(b.author_name) like %?1% "
            + "or cast(b.price as varchar) like %?1% "
            + "or lower(b.isbn) like %?1% or lower(b.language_code) like %?1% ", nativeQuery = true)
    List<Book> findAllByAttributes(String keyword, Pageable pageable);

    List<Book> findAll();

    Optional<Book> findByIsbn(String isbn);
}
