package com.bootcamp.bookshop.book;

import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import com.bootcamp.bookshop.exception.BookException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(final BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public List<BookView> getBookList(@RequestParam(required = false) final String sortBy,
                                      @RequestParam(required = false) final String order,
                                      @RequestParam(required = false) final String pageSize,
                                      @RequestParam(required = false) final String pageNumber,
                                      @RequestParam(required = false) final String search) {

        if (search != null) {
            return bookService.findAllByAttributes(search,
                    sortBy == null ? "price" : sortBy,
                    order == null ? "desc" : order,
                    pageNumber == null ? 1 : parseInt(pageNumber),
                    pageSize == null ? 10 : parseInt(pageSize));
        }

        return bookService.fetchAllBookView(sortBy == null ? "price" : sortBy,
                order == null ? "desc" : order,
                pageNumber == null ? 1 : parseInt(pageNumber),
                pageSize == null ? 10 : parseInt(pageSize));
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getBookById(@PathVariable final long id) {
        return bookService.findBookById(id);
    }

    @PostMapping("/admin/books")
    public ResponseEntity saveBooks(@RequestBody final List<CreateBookCommand> books) {
        try {
            bookService.create(books);
            return ResponseEntity.status(CREATED).body("");
        } catch (BookException | JsonProcessingException e) {
            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(e.getMessage());
        }
    }


    @PostMapping("/admin/books/upload")
    public ResponseEntity uploadBooks(@RequestParam("file") final MultipartFile file) throws IOException {

        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

            final CsvToBean<CreateBookCommand> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(CreateBookCommand.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            final List<CreateBookCommand> books = csvToBean.parse();

            bookService.create(books);
            return ResponseEntity.status(CREATED).body("");
        } catch (BookException | JsonProcessingException e) {
            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(e.getMessage());
        }
    }


}
