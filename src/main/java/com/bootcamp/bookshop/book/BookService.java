package com.bootcamp.bookshop.book;

import com.bootcamp.bookshop.exception.BookException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Validator;
import lombok.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    private final BookRepository bookRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Validator validator;

    @Autowired
    public BookService(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> fetchAll() {

        return bookRepository.findAll(Sort.by("price").descending());
    }

    public List<Book> fetchAll(final String sortBy, final String order) {
        if (order.equals("desc")) {
            return bookRepository.findAll(Sort.by(sortBy).descending());
        }
        return bookRepository.findAll(Sort.by(sortBy).ascending());
    }

    @Generated
    public Optional<Book> getByName(final String keyword) {
        return bookRepository.findByName(keyword);
    }

    public List<BookView> findAllByAttributes(final String word,
                                              final String sortBy, final String order,
                                              final int pageNumber, final int pageSize) {

        final List<Book> books;
        final String keyword = word.toLowerCase();

        if (order.equals("asc")) {
            books = bookRepository.findAllByAttributes(keyword,
                    PageRequest.of(pageNumber - 1, pageSize, Sort.by(sortBy).ascending()));
        } else {
            books = bookRepository.findAllByAttributes(keyword,
                    PageRequest.of(pageNumber - 1, pageSize, Sort.by(sortBy).descending()));
        }

        final List<BookView> bookView = new ArrayList<>();
        for (Book book : books) {
            bookView.add(book.toView(book.getId()));
        }
        return bookView;
    }

    public List<BookView> fetchAllBookView(final String sortBy, final String order,
                                           final int pageNumber, final int pageSize) {
        final List<Book> books;
        if (order.equals("asc")) {
            books = bookRepository.findAll(PageRequest.of(pageNumber - 1,
                    pageSize, Sort.by(sortBy).ascending())).getContent();
        } else {
            books = bookRepository.findAll(PageRequest.of(pageNumber - 1,
                    pageSize, Sort.by(sortBy).descending())).getContent();
        }

        final List<BookView> booksWithBookViewOnly = new ArrayList<>();
        for (Book book : books) {
            booksWithBookViewOnly.add(book.toView(book.getId()));
        }
        return booksWithBookViewOnly;
    }

    @Transactional(rollbackOn = BookException.class)
    public void create(final List<CreateBookCommand> books) throws BookException, JsonProcessingException {
        for (CreateBookCommand eachBook : books) {
            try {
                final String eachBookIsbn = eachBook.getIsbn();
                final Optional<Book> existingBook = bookRepository.findByIsbn(eachBookIsbn);
                if (existingBook.isPresent()) {
                    eachBook.setNewPriceAndBookCount(existingBook);
                    continue;
                }
                final Book book = Book.create(eachBook);
                validator.validate(book);
                bookRepository.save(book);
            } catch (Exception e) {
                throw new BookException("Exception :" + e.getMessage() + "\nFor Book : "
                        + objectMapper.writeValueAsString(eachBook) + "\nRollBack All Entries");
            }
        }
    }

    @Generated
    public Optional<Book> getById(final long bookId) {
        return bookRepository.findById(bookId);
    }


    public Optional<Book> findBookById(final long id) {
        return bookRepository.findById(id);
    }

}
