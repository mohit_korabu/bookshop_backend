package com.bootcamp.bookshop.book;

import com.bootcamp.bookshop.exception.CreateBookException;
import com.bootcamp.bookshop.exception.NoPriceException;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "books", schema = "PUBLIC")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String name;
    @Column(name = "author_name")
    @NotBlank
    private String authorName;
    @Min(value = 1, message = "Price cannot be zero")
    private Integer price;
    @NotBlank
    private String imageUrl;
    @NotBlank
    private String smallImageUrl;
    private int booksCount;
    @NotBlank
    private String isbn;
    @Column(name = "isbn_a")
    private String isbnA;
    @Column(name = "original_publication_year")
    private String originalPublicationYear;
    private String originalTitle;
    @Column(name = "language_code")
    private String languageCode;
    private float ratings;
    private String reviews;


    public void setPrice(final Integer price) {
        this.price = price;
    }

    public Book() {
    }

    public Book(final String name, final String authorName, final Integer price) {
        this(name, authorName, price, "Null", "Null", 0, Integer.toString((int) Math.random()),
                "0", "Null", "Null", "Null");
    }

    public Book(final String name, final String authorName, final int price, final String imageUrl,
                final String smallImageUrl, final int booksCount, final String isbn, final String isbnA,
                final String originalPublicationYear, final String originalTitle, final String languageCode) {
        this(name, authorName, price, imageUrl, smallImageUrl, booksCount, isbn,
                isbnA, originalPublicationYear, originalTitle, languageCode, 3.50F, "haga");
    }

    public Book(final String name, final String authorName, final int price, final String imageUrl,
                final String smallImageUrl, final int booksCount, final String isbn, final String isbnA,
                final String originalPublicationYear, final String originalTitle, final String languageCode,
                final float ratings, final String reviews) {
        this.name = name;
        this.authorName = authorName;
        this.price = price;
        this.imageUrl = imageUrl;
        this.smallImageUrl = smallImageUrl;
        this.booksCount = booksCount;
        this.isbn = isbn;
        this.isbnA = isbnA;
        this.originalPublicationYear = originalPublicationYear;
        this.originalTitle = originalTitle;
        this.languageCode = languageCode;
        this.ratings = ratings;
        this.reviews = "";
    }

    public BookView toView() {
        return BookView.builder()
                .name(name)
                .authorName(authorName)
                .price(price)
                .imageUrl(imageUrl)
                .build();
    }

    public BookView toView(final long bookId) {
        return BookView.builder()
                .id(bookId)
                .name(name)
                .authorName(authorName)
                .price(price)
                .imageUrl(imageUrl)
                .build();
    }


    public String getName() {
        return name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public Integer getPrice() {
        return price;
    }

    public Long getId() {
        return id;
    }


    public static Book create(final CreateBookCommand bookCommand) throws CreateBookException {
        if (bookCommand.getIsbn().isEmpty()) {
            throw new CreateBookException();
        }
        if (bookCommand.getPrice() == 0) {
            throw new NoPriceException("price cannot be zero");
        }
        return new Book(bookCommand.getName(), bookCommand.getAuthorName(), bookCommand.getPrice(),
                bookCommand.getImageUrl(), bookCommand.getSmallImageUrl(), bookCommand.getBooksCount(),
                bookCommand.getIsbn(), bookCommand.getIsbnA(), bookCommand.getOriginalPublicationYear(),
                bookCommand.getOriginalTitle(), bookCommand.getLanguageCode(), bookCommand.getRatings(),
                "");
    }

}
