package com.bootcamp.bookshop.book;

import com.bootcamp.bookshop.exception.UpdateBookException;
import com.opencsv.bean.CsvBindByName;
import java.util.Optional;
import javax.validation.constraints.NotBlank;
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class CreateBookCommand {
    @CsvBindByName(column = "title")
    @NotBlank
    private String name;
    @NotBlank
    @CsvBindByName(column = "author")
    private String authorName;
    @NotBlank
    @CsvBindByName
    private int price;
    @NotBlank
    @CsvBindByName(column = "image_url")
    @NotBlank
    private String imageUrl;
    @NotBlank
    @CsvBindByName(column = "small_image_url")
    private String smallImageUrl;
    @CsvBindByName(column = "books_count")
    private int booksCount;
    @NotBlank
    @CsvBindByName
    private String isbn;
    @NotBlank
    @CsvBindByName(column = "isbn13")
    private String isbnA;
    @CsvBindByName(column = "original_publication_year")
    private String originalPublicationYear;
    @CsvBindByName(column = "original_title")
    private String originalTitle;
    @CsvBindByName(column = "language_code")
    private String languageCode;
    @CsvBindByName
    private float ratings;

    public void setNewPriceAndBookCount(final Optional<Book> existingBook) throws UpdateBookException {
        try {
            existingBook.ifPresent(book -> {
                final int previousCount = book.getBooksCount();
                book.setBooksCount(booksCount + previousCount);
            });
            existingBook.ifPresent(book -> book.setPrice(price));
        } catch (Exception e) {
            throw new UpdateBookException("Cannot set price");
        }
    }
}