package com.bootcamp.bookshop.order;

import com.bootcamp.bookshop.book.Book;
import java.util.Optional;
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class CreateOrderCommand {
    private String name;
    private String email;
    private String mobileNumber;
    private String address;
    private String country;
    private Long bookId;
    private Integer bookCount;

    public void checkBookAvailability(final Optional<Book> actualBook) throws Exception {
        final Integer actualBookCount = actualBook.get().getBooksCount();
        if (actualBookCount < bookCount) {
            throw new Exception("Book out of stock , Available : " + actualBookCount.toString());
        }
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' + ", email='" + email + '\'' + ", mobileNumber='"
                + mobileNumber + '\'' + ", address='" + address + '\''
                + ", country='" + country + '\'';
    }

}
