package com.bootcamp.bookshop.order;

import com.bootcamp.bookshop.book.Book;
import lombok.Getter;

@Getter
public class OrderSummaryView {
    private final String name;
    private final String email;
    private final String mobileNumber;
    private final String address;
    private final String country;
    private final Long bookId;
    private final Integer bookCount;
    private final String bookName;
    private final String imageUrl;
    private final Integer totalAmount;
    private final Integer price;
    private final Boolean confirmation;

    public OrderSummaryView(final Order order,final Book book) {
        this.name = order.getName();
        this.email = order.getEmail();
        this.mobileNumber = order.getMobileNumber();
        this.address = order.getAddress();
        this.country = order.getCountry();
        this.bookId = order.getBookId();
        this.bookCount = order.getBookCount();
        this.bookName = book.getName();
        this.imageUrl = book.getImageUrl();
        this.totalAmount = order.getBookCount() * book.getPrice();
        this.price = book.getPrice();
        this.confirmation = order.isConfirmation();
    }
}
