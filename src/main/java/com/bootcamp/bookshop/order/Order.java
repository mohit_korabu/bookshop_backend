package com.bootcamp.bookshop.order;

import javax.persistence.*;
import lombok.Getter;

@Getter
@Entity
@Table(name = "orders", schema = "PUBLIC")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    @Column(name = "mobile_number")
    private String mobileNumber;
    private String address;
    private String country;
    @Column(name = "book_id")
    private Long bookId;
    @Column(name = "book_count")
    private Integer bookCount;
    private boolean confirmation;

    public Order() {
    }

    public Order(final String name, final String email, final String mobileNumber, final String address,
                 final String country, final long bookId, final Integer bookCount) {
        this(name, email, mobileNumber, address, country, bookId, bookCount, false);
    }

    public Order(final String name, final String email, final String mobileNumber, final String address,
                 final String country, final long bookId, final Integer bookCount, final boolean confirmation) {
        this.name = name;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.country = country;
        this.bookId = bookId;
        this.bookCount = bookCount;
        this.confirmation = confirmation;

    }

    public static Order create(final CreateOrderCommand order) {
        return new Order(order.getName(), order.getEmail(), order.getMobileNumber(), order.getAddress(),
                order.getCountry(), order.getBookId(), order.getBookCount(), false);
    }

    public void setConfirmation(final boolean confirmation) {
        this.confirmation = confirmation;
    }
}
