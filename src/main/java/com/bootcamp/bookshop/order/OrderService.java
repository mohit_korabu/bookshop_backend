package com.bootcamp.bookshop.order;

import com.bootcamp.bookshop.book.Book;
import com.bootcamp.bookshop.book.BookRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final BookRepository bookRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Validator validator;

    @Autowired
    public OrderService(final OrderRepository orderRepository, final BookRepository bookRepository) {
        this.orderRepository = orderRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public OrderSummary create(final CreateOrderCommand order) throws Exception, JsonProcessingException {
        try {
            final Optional<Book> actualBook = bookRepository.findById(order.getBookId());
            if (!actualBook.isPresent()) {
                throw new Exception("Invalid Book");
            }
            order.checkBookAvailability(actualBook);
            final Order newOrder = Order.create(order);
            validator.validate(newOrder);
            final Order saveOrder = orderRepository.save(newOrder);
            final Long orderId = saveOrder.getId();

            final String orderSummary = "Order Id = " + orderId.toString() + "\n" + order.toString()
                    + "\nBook Name = " + actualBook.get().getName() + "\nBook Count = " + order.getBookCount();
            return new OrderSummary(orderId, order.getName(), order.getEmail(), order.getMobileNumber(),
                    order.getAddress(), order.getCountry(), actualBook.get().getName(), order.getBookCount(),
                    actualBook.get().getPrice() * order.getBookCount());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<Order> fetchAll() {
        return orderRepository.findAll();
    }

    @Transactional
    public void confirmOrder(final Long orderId) throws Exception {
        final Optional<Order> order = orderRepository.findById(orderId);
        if (!order.isPresent()) {
            throw new Exception("Order Not Found");
        }
        if (order.get().isConfirmation()) {
            throw new Exception("Order Already Confirmed");
        }
        order.ifPresent(newOrder -> newOrder.setConfirmation(true));
        final Long actualBookId = order.get().getBookId();
        final Optional<Book> book = bookRepository.findById(actualBookId);
        final Integer actualBookCount = book.get().getBooksCount();
        final Integer orderBookCount = order.get().getBookCount();
        if (orderBookCount > actualBookCount) {
            throw new Exception("Late confirmation ,Book out of stock");
        }
        book.ifPresent(newBook -> newBook.setBooksCount(actualBookCount - orderBookCount));
    }

    public OrderSummaryView findOrderById(final long id) throws Exception {

        final Optional<Order> order = orderRepository.findById(id);

        if (order.isPresent()) {
            final Order newOrder = new Order(order.get().getName(),
                    order.get().getEmail(),
                    order.get().getMobileNumber(),
                    order.get().getAddress(),
                    order.get().getCountry(),
                    order.get().getBookId(),
                    order.get().getBookCount(),
                    order.get().isConfirmation());
            final  Optional<Book> book = bookRepository.findById(order.get().getBookId());
            if (book.isPresent()) {
                final Book newBook = new Book(book.get().getName(),
                        book.get().getAuthorName(),
                        book.get().getPrice(),
                        book.get().getImageUrl(),
                        book.get().getSmallImageUrl(),
                        book.get().getBooksCount(),
                        book.get().getIsbn(),
                        book.get().getIsbnA(),
                        book.get().getOriginalPublicationYear(),
                        book.get().getOriginalTitle(),
                        book.get().getLanguageCode(),
                        book.get().getRatings(),
                        book.get().getReviews());
                return new OrderSummaryView(newOrder, newBook);
            }
        }
        throw new Exception("Order Not Found");
    }

}
