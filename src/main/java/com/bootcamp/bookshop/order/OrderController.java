package com.bootcamp.bookshop.order;

import static org.springframework.http.HttpStatus.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(final OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/books/order")
    public ResponseEntity createOrder(@RequestBody final CreateOrderCommand order) {
        try {
            final OrderSummary orderSummary = orderService.create(order);
            return ResponseEntity.status(CREATED).body(orderSummary);
        } catch (Exception e) {
            final OrderMessage orderMessage = new OrderMessage(e.getMessage());
            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(orderMessage);
        }
    }

    @PutMapping("/books/order/{orderId}/confirm")
    public ResponseEntity confirmOrder(@PathVariable final Long orderId) {
        try {
            orderService.confirmOrder(orderId);
            final OrderMessage orderMessage = new OrderMessage("Order Confirmed");

            return ResponseEntity.status(OK).body(orderMessage);
        } catch (Exception e) {
            final OrderMessage orderMessage = new OrderMessage(e.getMessage());

            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(orderMessage);
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity getBookById(@PathVariable final long id) {
        try {

            return ResponseEntity.status(OK).body(orderService.findOrderById(id));
        } catch (Exception e) {
            final OrderMessage orderMessage = new OrderMessage(e.getMessage());

            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(orderMessage);
        }
    }

}
