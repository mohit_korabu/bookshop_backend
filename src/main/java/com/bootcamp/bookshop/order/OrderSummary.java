package com.bootcamp.bookshop.order;

import lombok.Getter;

@Getter
public class OrderSummary {
    private final Long orderId;
    private final String name;
    private final String email;
    private final String mobileNumber;
    private final String address;
    private final String country;
    private final String bookName;
    private final Integer bookCount;
    private final Integer totalAmount;

    public OrderSummary(final Long orderId,final String name, final String email,
                        final String mobileNumber, final String address,final String country,
                        final String bookName,final Integer bookCount,final Integer totalAmount) {
        this.orderId = orderId;
        this.name = name;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.country = country;
        this.bookName = bookName;
        this.bookCount = bookCount;
        this.totalAmount = totalAmount;
    }
}
