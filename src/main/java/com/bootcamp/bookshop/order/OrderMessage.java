package com.bootcamp.bookshop.order;

import lombok.Getter;

@Getter
public class OrderMessage {
    private final String message;

    public OrderMessage(final String message) {
        this.message = message;
    }
}
