package com.bootcamp.bookshop.exception;

import lombok.EqualsAndHashCode;
import lombok.Generated;

@EqualsAndHashCode(callSuper = false)
public class BookException extends Exception {

    @Generated
    public BookException() {
    }

    @Generated
    public BookException(final Throwable cause) {
        super(cause);
    }

    @Generated
    public BookException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BookException(final String message) {
        super(message);
    }
}
