package com.bootcamp.bookshop.exception;

import lombok.EqualsAndHashCode;
import lombok.Generated;

@EqualsAndHashCode(callSuper = false)
public class CreateBookException extends BookException {
    public CreateBookException(final String message) {
        super(message);
    }

    @Generated
    public CreateBookException() {
    }
}
