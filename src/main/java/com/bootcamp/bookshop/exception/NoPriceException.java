package com.bootcamp.bookshop.exception;

import lombok.EqualsAndHashCode;
import lombok.Generated;

@EqualsAndHashCode(callSuper = false)
public class NoPriceException extends CreateBookException {
    @Generated
    public NoPriceException() {
    }

    public NoPriceException(final String message) {
        super(message);
    }
}
