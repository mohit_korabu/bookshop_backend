package com.bootcamp.bookshop.exception;

import lombok.EqualsAndHashCode;
import lombok.Generated;

@EqualsAndHashCode(callSuper = false)
public class UpdateBookException extends BookException {
    @Generated
    public UpdateBookException(final String message) {
        super(message);
    }

    @Generated
    public UpdateBookException() {
    }
}
