package com.bootcamp.bookshop.error;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public class ErrorResponse {
    private final HttpStatus status;
    private final String message;
    private final Map<String, String> errors;

    @Generated
    public ErrorResponse(final HttpStatus status, final String message) {
        this.status = status;
        this.message = message;
        this.errors = new HashMap<>();
    }
}
