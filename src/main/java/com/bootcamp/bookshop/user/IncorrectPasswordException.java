package com.bootcamp.bookshop.user;

public class IncorrectPasswordException extends Exception {
    public IncorrectPasswordException() {
        super("Invalid credentials");
    }
}
