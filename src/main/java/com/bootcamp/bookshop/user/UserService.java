package com.bootcamp.bookshop.user;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Validator validator;

    public UserService() {
    }

    public User create(final CreateUserCommand userCommand)
            throws InvalidEmailException, InvalidPasswordFormatException {
        final Optional<User> user = userRepository.findByEmail(userCommand.getEmail());
        if (user.isPresent()) {
            throw new InvalidEmailException();
        } else if (!isValidPassword(userCommand.getPassword())) {
            throw new InvalidPasswordFormatException();
        }
        final User newUser = User.create(userCommand);
        validator.validate(newUser);
        return userRepository.save(newUser);
    }

    private static boolean isValidPassword(final String password) {
        final String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        final java.util.regex.Pattern pattern = Pattern.compile(regex);
        if (password == null) {
            return false;
        }
        final Matcher m = pattern.matcher(password);
        return m.matches();
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        final User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                AuthorityUtils.createAuthorityList(user.getRole().authority())
        );
    }

    public User signIn(final SignInUserCommand signInUserCommand) throws IncorrectPasswordException {
        final User user = userRepository.findByEmail(signInUserCommand.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        if (!isPasswordValid(signInUserCommand, user)) {
            throw new IncorrectPasswordException();
        }

        return user;
    }


    private boolean isPasswordValid(final SignInUserCommand signInUserCommand, final User user) {
        return User.PASSWORD_ENCODER.matches(signInUserCommand.getPassword(), user.getPassword());
    }
}