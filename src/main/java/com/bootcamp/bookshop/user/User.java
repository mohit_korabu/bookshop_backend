package com.bootcamp.bookshop.user;

import static javax.persistence.EnumType.STRING;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users", schema = "PUBLIC")
public class User {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Email is mandatory")
    @Email
    private String email;

    @NotBlank(message = "Password is mandatory")
    private String password;
    @Enumerated(STRING)
    private Role role;

    private User(final String email, final String password) {
        this(email, password, Role.USER);
    }

    public User(final String email, final String password, final Role role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public static User create(final CreateUserCommand userCommand) throws InvalidPasswordFormatException {
        String password = "";

        if (!userCommand.getPassword().isEmpty()) {
            password = PASSWORD_ENCODER.encode(userCommand.getPassword());
        }
        return new User(userCommand.getEmail(), password);
    }

    public UserView toView() {
        return UserView.builder()
                .id(id.toString())
                .email(email)
                .build();
    }
}
