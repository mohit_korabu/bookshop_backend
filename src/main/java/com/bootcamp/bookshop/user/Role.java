package com.bootcamp.bookshop.user;

public enum Role {
    ADMIN, USER;

    String authority() {
        return "ROLE_" + this.name();
    }
}
