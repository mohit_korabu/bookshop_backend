package com.bootcamp.bookshop.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @PostMapping
    ResponseEntity<UserView> create(@RequestBody final CreateUserCommand userCommand) {
        final User user;

        try {
            user = userService.create(userCommand);
        } catch (InvalidPasswordFormatException | InvalidEmailException e) {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(user.toView(), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    ResponseEntity<String> login(@RequestBody final SignInUserCommand signInUserCommand) {
        final User user;
        try {
            user = userService.signIn(signInUserCommand);
        } catch (IncorrectPasswordException e) {
            return ResponseEntity.badRequest()
                    .body("Incorrect Password");
        }

        if (user.getRole().equals(Role.ADMIN)) {
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .header(HttpHeaders.LOCATION, "/admin").build();
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .header(HttpHeaders.LOCATION, "/books").build();
    }

}
