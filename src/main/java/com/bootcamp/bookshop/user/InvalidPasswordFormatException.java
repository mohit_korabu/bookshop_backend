package com.bootcamp.bookshop.user;

public class InvalidPasswordFormatException extends Exception {
    public InvalidPasswordFormatException() {
        super("Invalid Password Format");
    }
}
