package com.bootcamp.bookshop.user;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
public class SignInUserCommand {
    @NotBlank(message = "Email is mandatory")
    private final String email;
    @NotBlank(message = "Password is mandatory")
    @NotNull(message = "Password is mandatory")
    private final String password;
    @Enumerated(STRING)
    private Role role;
}